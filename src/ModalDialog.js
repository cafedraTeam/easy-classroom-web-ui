import React, {Component} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTypes from "./data/constants/DialogTypes";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import RootStoreActions from "./data/flux/actions/RootStoreActions";
import InfoIcon from "@material-ui/icons/Info";
import ErrorIcon from "@material-ui/icons/Error";
import QuestionIcon from "@material-ui/icons/QuestionAnswer";
import Theme from "./Theme";

import red from '@material-ui/core/colors/red';
import lightGreen from '@material-ui/core/colors/lightGreen';

const RED_COLOR = red[300];
const GREEN_COLOR = lightGreen[300];

class ModalDialog extends Component {
    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.props.type === DialogTypes.CONFIRM ? {} : RootStoreActions.hideModalDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {this.props.type === DialogTypes.ERROR && <ErrorIcon style={{color: RED_COLOR}}/>}
                    {this.props.type === DialogTypes.INFO && <InfoIcon style={{color: 'blue'}}/>}
                    {this.props.type === DialogTypes.CONFIRM && <QuestionIcon style={{color: 'blue'}}/>}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {this.props.message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    {this.props.type !== DialogTypes.CONFIRM &&
                    <Button onClick={RootStoreActions.hideModalDialog} color="primary" autoFocus>
                        OK
                    </Button>}
                    {this.props.type === DialogTypes.CONFIRM &&
                    <div>
                        <Button style={{margin: Theme.spacing.unit, backgroundColor: GREEN_COLOR}}
                                onClick={() => {
                                    RootStoreActions.hideModalDialog();
                                    this.props.onYes();
                                }}
                                variant="contained"
                                autoFocus
                        >
                            Yes
                        </Button>
                        <Button style={{margin: Theme.spacing.unit, backgroundColor: RED_COLOR}}
                                onClick={() => {
                                    RootStoreActions.hideModalDialog();
                                    this.props.onNo();
                                }}
                                variant="contained"
                        >
                            No
                        </Button>
                    </div>
                    }
                </DialogActions>
            </Dialog>
        );
    }
}

export default ModalDialog;