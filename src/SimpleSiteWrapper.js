import React from 'react';
import styled from 'styled-components';

const Header = () => {

    const st = {
        root: {
            position: 'absolute',
            top: 0,
            left: '50%',
            transform: 'translate(-50%, 0)',

        }
    };

    return (<div>
    </div>)
};

const Page = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
`;

export default class SimpleSiteWrapper extends React.Component {
    render() {
        return (<Page>
            <div>
                    {this.props.children}
            </div>
        </Page>);
    }

}
