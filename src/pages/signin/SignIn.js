import React from 'react';
import CredentialsForm from "./CredentialsForm";
import SignInStore from "../../data/flux/stores/MainStore";
import StoreEvents from "../../data/flux/StoreEvents";

export default class SignIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = SignInStore.getStateJS();
    }


    componentDidMount = () => SignInStore.on(StoreEvents.MAIN_STORE_UPDATED, this.updateState);

    componentWillUnmount = () => SignInStore.removeListener(StoreEvents.MAIN_STORE_UPDATED, this.updateState);

    updateState = () => this.setState(SignInStore.getStateJS());

    render() {
        return <CredentialsForm credentials={this.state.credentials}/>
    }
}