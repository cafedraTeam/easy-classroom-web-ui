import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Theme from "../../Theme";
import Input from "@material-ui/core/Input";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import SignInStoreActions from "../../data/flux/actions/SignInStoreActions";
import MainStoreActions from "../../data/flux/actions/MainStoreActions";
import UserAPI from "../../data/api/UserAPI";

const styles = theme => ({
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});


function CredentialsForm(props) {

    const style = styles(Theme);

    return (
        <Paper style={style.paper}>
            <Avatar style={style.avatar}>
                <LockOutlinedIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">Sign in</Typography>
            <Paper style={style.form}>

                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="username">Enter your username or email</InputLabel>
                    <Input onChange={e => SignInStoreActions.setUsernameOrEmail(e.target.value)}
                           value={props.credentials.username}
                           id="username"
                           name="username"
                           autoComplete="email"
                           autoFocus/>
                </FormControl>

                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input onChange={e => SignInStoreActions.setPassword(e.target.value)}
                           name="password"
                           type="password"
                           id="password"
                           autoComplete="current-password"/>
                </FormControl>

                <FormControlLabel
                    control={<Checkbox onChange={e => console.log(e.target.checked)}
                                       value="remember"
                                       color="primary"/>}
                    label="Remember me"
                />
                }

                <Button
                    type="submit"
                    fullWidth
                    style={style.submit}
                    onClick={() => UserAPI.signIn(props.credentials)}
                >
                    Sign in
                </Button>
            </Paper>
        </Paper>
    );
}

CredentialsForm.defaultProps = {
    rememberMe: false,
    credentials: {
        username: '',
        password: '',
    },
};

export default CredentialsForm;