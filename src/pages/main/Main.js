import React from 'react';
import MainStore from "../../data/flux/stores/MainStore";
import StoreEvents from "../../data/flux/StoreEvents";
import CafedraMap from './components/CafedraMap';
import ClassroomAPI from '../../data/api/ClassroomAPI';
import ActionsAPI from '../../data/api/ActionsAPI';
import ActionsPanel from './components/ActionsPanel';
import VideoAndInfoPanel from "./components/VideoAndInfoPanel";
import ClassroomDialog from "./components/ClassroomsDialog";
import ConfirmDialog from "./components/ConfirmDialog";


export default class Main extends React.Component {

    webcam = React.createRef();

    constructor(props) {
        super(props);
        this.state = MainStore.getStateJS();
    }

    componentDidMount = () => {
        MainStore.on(StoreEvents.MAIN_STORE_UPDATED, this.updateState);
        ClassroomAPI.fetchClassrooms();
        ActionsAPI.fetchActions();
    };

    componentWillUnmount = () => MainStore.removeListener(StoreEvents.MAIN_STORE_UPDATED, this.updateState);

    updateState = () => this.setState(MainStore.getStateJS());

    render() {

        return <div style={{width: '100vw', display: 'flex'}}>
            <CafedraMap classrooms={this.state.classrooms}/>
            <VideoAndInfoPanel userInfo={this.state.lastRecognizedUser}
                               webcam={this.webcam}
                               setWebcam={webcam => this.webcam = webcam}
                               warn={this.state.warn}
                               videoConstraints={this.state.videoConstraints}/>
            <ActionsPanel actions={this.state.actions}/>

            <ClassroomDialog open={this.state.showClassroomsSelectDialog}
                             user={this.state.lastRecognizedUser}
                             classrooms={this.state.classrooms}/>
            <ConfirmDialog open={this.state.showConfirmDialog}
                           message={this.state.confirmDialogMessage}
                           onYes={this.state.onYes}
                           onNo={this.state.onNo}
            />

        </div>
    }
}
