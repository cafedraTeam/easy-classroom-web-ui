import React, {Component} from 'react';
import {Typography, Button} from "@material-ui/core";
import {Link} from "react-router-dom";
import MainStoreActions from "../../../data/flux/actions/MainStoreActions";
import {findMaxResolution} from "../../../data/utils/cameraUtils";
import Webcam from 'react-webcam';
import UserAPI from "../../../data/api/UserAPI";
import { borders } from '@material-ui/system';

const SHCEDULE_LINK = "//https://github.com/svyatoslav00012/ReactFaceCapture/blob/master/src/elems/CapturePhoto.jshttps://github.com/svyatoslav00012/ReactFaceCapture/blob/master/src/elems/CapturePhoto.js"

function InfoPanel(props) {
    console.log(props);
    return (<div style={{padding: 'auto'}}>
        <Typography variant="h3">Last recognized user</Typography>
        <Typography>{props.user.studNumber ? "studNumber : " + props.user.studNumber : "Teacher"}</Typography>
        <Typography>firstName : {props.user.firstName}</Typography>
        <Typography>firstName : {props.user.lastName}</Typography>
        {props.user.group && <Typography>group : {props.user.group}</Typography>}
    </div>);
}

class VideoAndInfoPanel extends Component {

    webcam = React.createRef();

    componentDidMount() {
        findMaxResolution()
            .then(res => MainStoreActions.updateVideoConstraints(res))
            .catch(err => console.error("error finding max resolution"));
        setInterval(() => {
            console.log(this.webcam);
            UserAPI.recognizeStudentId(this.webcam.getScreenshot())
        }, 500);
    }

    render() {

        const st = {
            root: {
                height: '100vh',
                width: '45%',
            },
        };

        // console.log(this.webcam);


        return (
            <div style={st.root}>
                
                <Webcam
                    style={{
                        width: '100%',
                        transform: 'scale(-1, 1)',
                    }}
                    minScreenshotHeight={this.props.videoConstraints.height}
                    minScreenshotWidth={this.props.videoConstraints.width}
                    ref={w => this.webcam = w}
                    audio={false}
                    screenshotFormat="image/jpeg"
                    videoConstraints={this.props.videoConstraints}
                    imageSmoothing={false}
                    screenshotQuality={1}/>
                
                {(this.props.userInfo && !this.props.warn) && <InfoPanel user={this.props.userInfo}/>}
                {(!this.props.userInfo && !this.props.warn) && <Typography>Put your student id in front of camera</Typography>}
                {this.props.warn && <Typography style={{color: 'red'}}>{this.props.warn}</Typography>}
                <div style={{display: 'flex', judtifyContent: 'center'}}>
                    <Button variant="contained" style={{margin: 10}} component={Link} to={SHCEDULE_LINK}>
                        shcedule</Button>
                       S
                </div>
            </div>
        );
    }
}



export default VideoAndInfoPanel;