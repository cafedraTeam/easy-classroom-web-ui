import React, {Component} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import MainStoreActions from "../../../data/flux/actions/MainStoreActions";
import UserAPI from "../../../data/api/UserAPI";

class ClassroomDialog extends Component {
    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={MainStoreActions.hideClassroomsSelectDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Choose classroom
                </DialogTitle>
                <DialogActions>
                    {this.props.classrooms
                        .filter(classroom => classroom.current_user_id === null)
                        .map(classroom =>
                            <Button
                                onClick={() => {
                                    MainStoreActions.showConfirmDialog(
                                        "Are you sure you want to take classroom " + classroom.number,
                                        () => {
                                            MainStoreActions.hideClassroomsSelectDialog();
                                            UserAPI.getKey(this.props.user.id, classroom.number);
                                        },
                                        MainStoreActions.hideClassroomsSelectDialog
                                    );
                                }}
                                color="primary"
                                autoFocus>
                                {classroom.number}
                            </Button>)
                    }
                    <Button fullWidth onClick={MainStoreActions.hideClassroomsSelectDialog}>Cancel</Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default ClassroomDialog;