import React from 'react';
import Paper from "@material-ui/core/Paper";
import {Typography} from "@material-ui/core";
import ClassroomAPI from "../../../data/api/ClassroomAPI";

import red from '@material-ui/core/colors/red';
import lightGreen from '@material-ui/core/colors/lightGreen';


const RED_COLOR = red[300];
const GREEN_COLOR = lightGreen[300];


const classroomStyle = {
    position: 'absolute',
    height: 120,
    width: 120,
    padding: '2%',
};

const classroomsPositions = {
    "123a": {top: '2%', left: ' 5%'},
    "123b": {top: '27%', left: '5%'},
    "124": {top: '2%', left: '55%'},
    "125": {top: '27%', left: '55%'},
    "126": {top: '52%', left: '55%'},
    "127": {top: '77%', left: '55%'}
};

export default class CafedraMap extends React.Component {

    componentDidMount() {
        setInterval(ClassroomAPI.fetchClassrooms, 10000)
    }

    render() {
        return (
            <div style={{
                width: '25%',
                height: '100vh'
            }}
            >
                <div style={{position: 'relative', width: '100%', height: '100%'}}>
                    {this.props.classrooms.map((classroom) =>
                        <Paper style={{
                            ...classroomStyle,
                            ...classroomsPositions[classroom.number],
                            backgroundColor: classroom.current_user_id ? RED_COLOR : GREEN_COLOR
                        }}>
                            <Typography variant="h6">{classroom.number}</Typography>
                        </Paper>
                        
                    )}
                </div>
            </div>
        );
    }
};
