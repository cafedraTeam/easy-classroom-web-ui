import React, {Component} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import QuestionIcon from "@material-ui/icons/QuestionAnswer";
import MainStoreActions from "../../../data/flux/actions/MainStoreActions";
import Theme from "../../../Theme";
import red from '@material-ui/core/colors/red';
import lightGreen from '@material-ui/core/colors/lightGreen';

const RED_COLOR = red[300];
const GREEN_COLOR = lightGreen[300];

class ConfirmDialog extends Component {
    render() {
        return (
            <Dialog
                open={this.props.open}
                aria-labelledby="confirm-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title"><QuestionIcon style={{color: 'blue'}}/></DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {this.props.message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <div>
                        <Button style={{margin: Theme.spacing.unit, backgroundColor: GREEN_COLOR}}
                                onClick={() => {
                                    MainStoreActions.hideConfirmDialog();
                                    this.props.onYes();
                                }}
                                variant="contained"
                                autoFocus
                        >
                            Yes
                        </Button>
                        <Button style={{margin: Theme.spacing.unit, backgroundColor: RED_COLOR }}
                                onClick={() => {
                                    MainStoreActions.hideConfirmDialog();
                                    this.props.onNo();
                                }}
                                variant="contained"
                        >
                            No
                        </Button>
                    </div>
                </DialogActions>
            </Dialog>
        );
    }
}

export default ConfirmDialog;