import React from 'react';
import RequestTypes from '../../../data/constants/RequestTypes';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import lightGreen from '@material-ui/core/colors/lightGreen';
import Button from "@material-ui/core/Button";
import ActionsAPI from "../../../data/api/ActionsAPI";
import ClassroomAPI from "../../../data/api/ClassroomAPI";

const RED_COLOR = red[300];
const GREEN_COLOR = lightGreen[300];

function Action(props) {

    const actionStyle = {
        position: 'relative',
        height: '100px',
        width: '100%',
        margin: 10,
        padding: 10,
        backgroundColor: props.color,
        borderRadius: 10
    };

    const timeoptions = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        milisecond: 'numeric'
    };

    return (<Paper style={actionStyle}>
        <Typography style={{position: 'absolute', left: 5, top: 5}}>{props.classroom}</Typography>
        <Typography style={{position: 'absolute', left: '5', top: 50}}>
            {new Intl.DateTimeFormat('ukr', timeoptions).format(props.timestamp * 1000)}
        </Typography>
        <Typography style={{position: 'absolute', left: '20vw', top: 5}}>{props.user}</Typography>
        <Typography style={{position: 'absolute', left: '20vw', top: 50}}>{props.description}</Typography>

    </Paper>);

}

export default class ActionsPanel extends React.Component {

    componentDidMount() {
        setInterval(ClassroomAPI.fetchActions, 10000);
    }

    render() {

        return (<div style={{height: '100vh', width: '30%'}}>
            <div style={{height: 'calc(100vh - 50px)', overflowY: 'auto'}}>
                {this.props.actions
                    .sort((a, b) => a.timestamp < b.timestamp ? -1 : a.timestamp > b.timestamp ? 1 : 0)
                    .map(
                        action => {

                            const user = action.user;
                            console.log(action)
                            //const user = action.user.last_name + " " + action.user.first_name + " " + action.user.group
                            const classroom = action.classroom_id;
                            const description = action.type === RequestTypes.GET_KEY ? "got key" : "returned key";


                            //.toDateString()
                            const time = action.timestamp//new Intl.DateTimeFormat('ukr', timeoptions).format(action.timestamp);
                            const color = action.type === RequestTypes.GET_KEY ? RED_COLOR : GREEN_COLOR;

                            return (<Action classroom={classroom}
                                            user={user}
                                            description={description}
                                            timestamp={time}
                                            color={color}/>);

                        }
                    )}

            </div>
            <Button variant="contained" style={{margin: 10}} onClick={() =>{
                ActionsAPI.fetchActions();
                ClassroomAPI.fetchClassrooms();
            }}>Refresh</Button>
        </div>)

    }
}