import React from 'react';
import ReactDOM from 'react-dom';
import AppRoutes from "./AppRoutes";

document.body.style.margin = "0";
document.body.style.width = '100vw';
document.body.style.height = '100vh';

ReactDOM.render(<AppRoutes/>, document.getElementById('root'));
