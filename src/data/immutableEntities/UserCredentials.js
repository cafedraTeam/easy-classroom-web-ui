import Immutable from 'immutable';

const UserCredentials = Immutable.Record({
    usernameOrEmail: "",
    password: "",
});

export default UserCredentials;