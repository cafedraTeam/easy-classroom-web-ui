import Immutable from 'immutable';

const User = Immutable.Record({
    studNumber: '',
    firstName: '',
    lastName: '',
    group: '',
    email: '',
    phoneNumber: '',
    role: ''
});

export default User;