class JsonUtils {
    userBackToFront(userFromBackend){
        return {
            id: userFromBackend.id,
            studNumber: userFromBackend.stud_number,
            firstName: userFromBackend.first_name,
            lastName: userFromBackend.last_name,
            group: userFromBackend.group,
            email: userFromBackend.email,
            phoneNumber: userFromBackend.phone_number,
            role: userFromBackend.role
        }
    }
}

export default new JsonUtils();