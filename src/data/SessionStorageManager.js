const SessionStorageItems = {
    CURRENT_USER: "CURRENT_USER"
};

class SessionStorageManager {

    isAuthenticated = () => this.getCurrentUser() !== null;

    getCurrentUser = () => JSON.parse(sessionStorage.getItem(SessionStorageItems.CURRENT_USER));

    setCurrentUser = (user) => sessionStorage.setItem(SessionStorageItems.CURRENT_USER, JSON.stringify(user));

}

export default new SessionStorageManager();