const DialogTypes = {
    INFO: "INFO",
    ERROR: "ERROR",
    CONFIRM: "CONFIRM"
};

export default DialogTypes;