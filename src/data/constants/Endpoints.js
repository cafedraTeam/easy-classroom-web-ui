const DOMAIN_URL = "";

const Endpoints = {
    USER_SIGN_IN: DOMAIN_URL + "/api/user/signin",
    USER_SIGN_OUT: DOMAIN_URL + "/api/user/signout",

    RECOGNIZE_STUDENT_ID: DOMAIN_URL + "/api/recognize",

    CLASSROOM_GET_KEY: DOMAIN_URL + "/api/get",
    CLASSROOM_PUT_KEY: DOMAIN_URL + "/api/put",

    ACTION_GET_ALL: DOMAIN_URL + "/api/actions/getAll",
    FETCH_CLASSROOMS: DOMAIN_URL + "/api/classrooms/getAll",
};

export default Endpoints;