const RequestTypes = {
    GET_KEY: "GET_KEY",
    PUT_KEY: "PUT_KEY"
}

export default RequestTypes;