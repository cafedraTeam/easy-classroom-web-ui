import axios from "axios";
import Endpoints from "../constants/Endpoints";
import MainStoreActions from "../flux/actions/MainStoreActions";

class ActionsAPI {

    fetchActions = () => {
        axios.get(Endpoints.ACTION_GET_ALL)
            .then(result => MainStoreActions.setActions(result.data))
            .catch(error => console.error(error));
    };


}

export default new ActionsAPI();