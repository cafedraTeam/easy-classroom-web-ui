import axios from "axios";
import Endpoints from "../constants/Endpoints";
import MainStoreActions from "../flux/actions/MainStoreActions";
import RootStoreActions from "../flux/actions/RootStoreActions";

class ClassroomAPI {

    fetchClassrooms = () => {
      axios.get(Endpoints.FETCH_CLASSROOMS)
      .then(result => {
          MainStoreActions.setClassrooms(result.data)
      })
      .catch(err => RootStoreActions.showErrorDialog("Error fetching classrooms", err));
    }

}

export default new ClassroomAPI();
