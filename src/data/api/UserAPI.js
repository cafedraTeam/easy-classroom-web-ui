import axios from "axios";
import Endpoints from "../constants/Endpoints";
import MainStoreActions from "../flux/actions/MainStoreActions";
import JsonUtils from "../utils/JsonUtils";
import ClassroomAPI from "./ClassroomAPI";
import ActionsAPI from "./ActionsAPI";

class UserAPI {

    signIn = (credentials) => {
        axios.put(Endpoints.USER_SIGN_IN, credentials)
            .then(result => console.log(result))
            .catch(error => console.error(error));
    };

    signOut = (credentials) => {
        axios.post(Endpoints.USER_SIGN_OUT, credentials)
            .then(result => console.log(result))
            .catch(error => console.error(error));
    };

    recognizeStudentId = (src) => {
        axios.put(Endpoints.RECOGNIZE_STUDENT_ID, {img: src})
            .then(result => {
                switch(result.data) {
                    case "not recognized":
                        console.log(result.data);
                        break;
                    case "user doest not exist":
                        MainStoreActions.setWarn(result.data);
                        break;
                    default:
                        const parsedObject = JSON.parse(result.data)[0];
                        const user = JsonUtils.userBackToFront(
                            {...parsedObject.fields, id: parsedObject.pk}
                            );
                        MainStoreActions.setLastRecognizedUser(user);

                }
            })
            .catch(error => console.error(error));
    };

    getKey = (userId, classroomNumber) =>
        axios.put(Endpoints.CLASSROOM_GET_KEY,
            {user_id: userId, classroom_number: classroomNumber, timestamp: new Date().getTime()})
            .then(result => {
                console.log(result.data);
                ClassroomAPI.fetchClassrooms();
                ActionsAPI.fetchActions();
            })
            .catch(error => console.error(error));

    returnKey = (userId, classroomNumber) =>
        axios.put(Endpoints.CLASSROOM_PUT_KEY,
            {user_id: userId, classroom_number: classroomNumber, timestamp: new Date().getTime()})
            .then(result => {
                console.log(result.data);
                ClassroomAPI.fetchClassrooms();
                ActionsAPI.fetchActions();
            })
            .catch(error => console.error(error));
}

export default new UserAPI();