const prefix = "ROOT_";

const RootStoreActionTypes = {
    SET_AUTHENTICATED_USER: prefix + "SET_AUTHENTICATED_USER",
    SHOW_MODAL_DIALOG: prefix + "SHOW_MODAL_DIALOG",
    HIDE_MODAL_DIALOG: prefix + "HIDE_MODAL_DIALOG",
};

export default RootStoreActionTypes;
