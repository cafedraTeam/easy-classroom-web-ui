const prefix = "SIGN_IN_";

const SignInStoreActionTypes = {
    SET_USERNAME_OR_EMAIL: prefix + "SET_USERNAME_OR_EMAIL",
    SET_PASSWORD: prefix + "SET_PASSWORD",
};

export default SignInStoreActionTypes;