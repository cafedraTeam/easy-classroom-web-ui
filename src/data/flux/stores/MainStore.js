import {EventEmitter} from "events";
import StoreEvents from "../StoreEvents";
import AppDispatcher from "../AppDispatcher";
import Immutable from "immutable";
import MainStoreActionTypes from "../actionTypes/MainStoreActionTypes";
import UserAPI from "../../api/UserAPI";


class MainStore extends EventEmitter {

    constructor() {
        super();
        this.prevStateJsStringified = "";
        this.state = new Immutable.OrderedMap({
            classrooms: [],
            actions: [],
            showClassroomsSelectDialog: false,
            showConfirmDialog: false,
            confirmDialogMessage: '',
            onYes: () => {
            },
            onNo: () => {
            },
            lastRecognizedUser: null,
            videoConstraints: {
                width: 1280,
                height: 720,
                frameRate: 60,
                facingMode: "user",
            },
            warn: null
        });
    }

    handleAction = (action) => {
        this.prevStateJsStringified = JSON.stringify(this.state.toJS());
        switch (action.type) {
            case MainStoreActionTypes.SET_CLASSROOMS:
                this.state = this.state.set("classrooms", action.classrooms);
                break;

            case MainStoreActionTypes.SET_ACTIONS:
                this.state = this.state.set("actions", action.actions);
                break;

            case MainStoreActionTypes.SHOW_CLASSROOMS_SELECT_DIALOG:
                this.state = this.state.set("showClassroomsSelectDialog", true);
                break;

            case MainStoreActionTypes.HIDE_CLASSROOMS_SELECT_DIALOG:
                this.state = this.state.set("showClassroomsSelectDialog", false);
                break;

            case MainStoreActionTypes.SHOW_CONFIRM_DIALOG:
                this.state = this.state
                    .set("showConfirmDialog", true)
                    .set("confirmDialogMessage", action.message)
                    .set("onYes", action.onYes)
                    .set("onNo", action.onNo);
                break;

            case MainStoreActionTypes.HIDE_CONFIRM_DIALOG:
                this.state = this.state.set("showConfirmDialog", false);
                break;

            case MainStoreActionTypes.UPDATE_VIDEO_CONSTRAINTS:
                this.state = this.state
                    .set("videoConstraints", {...this.state.get("videoConstraints"), ...action.videoConstraints});
                break;

            case MainStoreActionTypes.SET_LAST_RECOGNIZED_USER:
                if (action.user === this.state.get("lastRecognizedUser"))
                    return;
                this.handleNewUser(action.user);
                break;

            case MainStoreActionTypes.SET_WARN:
                this.state = this.state.set("warn", action.warn);
                break;
        }
        this.checkStateAndEmmit();
    };

    handleNewUser = user => {
        const classroom = this.state.get("classrooms").find(c => c.current_user_id === user.id);
        if (classroom !== undefined && classroom !== null)
            this.state = this.state
                .set("showConfirmDialog", true)
                .set("confirmDialogMessage", "Do you really want to return key from " + classroom.number)
                .set("onYes", () => UserAPI.returnKey(user.id, classroom.number))
                .set("onNo", () => console.log("canceled"));
        else {
            this.state = this.state.set("showClassroomsSelectDialog", true);
        }
        this.state = this.state
            .set("lastRecognizedUser", user)
            .set("warn", null);
    };

    checkStateAndEmmit = () => {
        const currentStateJsStringified = JSON.stringify(this.state.toJS());
        if (currentStateJsStringified !== this.prevStateJsStringified)
            this.emit(StoreEvents.MAIN_STORE_UPDATED);
    };

    getStateJS = () => this.state.toJS();
}

const mainStore = new MainStore();
mainStore.token = AppDispatcher.register(mainStore.handleAction);
export default mainStore;
