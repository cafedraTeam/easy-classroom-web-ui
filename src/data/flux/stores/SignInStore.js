import SignInStoreActionTypes from "../actionTypes/SignInStoreActionTypes";
import UserCredentials from "../../immutableEntities/UserCredentials";
import {EventEmitter} from "events";
import StoreEvents from "../StoreEvents";
import AppDispatcher from "../AppDispatcher";
import Immutable from 'immutable';
import MainStoreActionTypes from "../actionTypes/MainStoreActionTypes";

let token;

class SignInStore extends EventEmitter {

    constructor() {
        super();
        this.prevStateJsStringified = "";
        this.state = new Immutable.OrderedMap({
            credentials: new UserCredentials(),
        });
    }

    handleAction = (action) => {
        this.prevStateJsStringified = JSON.stringify(this.state.toJS());
        switch (action.type) {
            case MainStoreActionTypes.SET_USERNAME_OR_EMAIL:
                this.state = this.state.set("credentials", this.state.get("credentials").set("usernameOrEmail", action.usernameOrEmail));
                break;
            case MainStoreActionTypes.SET_PASSWORD:
                this.state = this.state.set("credentials", this.state.get("credentials").set("password", action.password));
                break;

        }
        this.checkStateAndEmmit();
    };

    checkStateAndEmmit = () => {
        const currentStateJsStringified = JSON.stringify(this.state.toJS());
        if (currentStateJsStringified !== this.prevStateJsStringified)
            this.emit(StoreEvents.SIGN_IN_STORE_UPDATED);
    };

    getStateJS = () => this.state.toJS();
}

const signInStore = new SignInStore();
token = AppDispatcher.register(signInStore.handleAction);
export default signInStore;