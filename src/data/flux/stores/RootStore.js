import {EventEmitter} from "events";
import StoreEvents from "../StoreEvents";
import AppDispatcher from "../AppDispatcher";
import Immutable from "immutable";
import DialogTypes from "../../constants/DialogTypes";
import RootStoreActionTypes from "../actionTypes/RootStoreActionTypes";

class RootStore extends EventEmitter {

    constructor() {
        super();
        this.prevStateJsStringified = "";
        this.state = new Immutable.OrderedMap({
            authenticatedUser: null,
            showModalDialog: false,
            dialogType: DialogTypes.INFO,
            message: '',
            onYes: null,
            onNo: null,
        });
    }

    handleAction = (action) => {
        this.prevStateJsStringified = JSON.stringify(this.state.toJS());
        switch (action.type) {
            case RootStoreActionTypes.SET_AUTHENTICATED_USER:
                this.state = this.state.set("authenticatedUser", action.user);
                break;
            case RootStoreActionTypes.SHOW_MODAL_DIALOG:
                this.state =
                    this.state.set("showModalDialog", true)
                        .set("dialogType", action.dialogType)
                        .set("message", action.message)
                        .set("onYes", action.onYes)
                        .set("onNo", action.onNo);
                break;
            case RootStoreActionTypes.HIDE_MODAL_DIALOG:
                this.state = this.state.set("showModalDialog", false);
                console.log(this.state.toJS().showModalDialog);
                break;
        }
        this.checkStateAndEmmit();
    };

    checkStateAndEmmit = () => {
        const currentStateJsStringified = JSON.stringify(this.state.toJS());
        if (currentStateJsStringified !== this.prevStateJsStringified)
            this.emit(StoreEvents.ROOT_STORE_UPDATED);
    };

    getStateJS = () => this.state.toJS();
}

const rootStore = new RootStore();
rootStore.token = AppDispatcher.register(rootStore.handleAction);
export default rootStore;
