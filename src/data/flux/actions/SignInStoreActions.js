import AppDispatcher from "../AppDispatcher";
import SignInStoreActionTypes from "../actionTypes/SignInStoreActionTypes";
import MainStoreActionTypes from "../actionTypes/MainStoreActionTypes";

const SignInStoreActions = {
    setUsernameOrEmail(usernameOrEmail) {
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SET_USERNAME_OR_EMAIL,
            usernameOrEmail
        });
    },

    setPassword(password) {
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SET_PASSWORD,
            password
        });
    },
};

export default SignInStoreActions;