import AppDispatcher from "../AppDispatcher";
import MainStoreActionTypes from "../actionTypes/MainStoreActionTypes";
import DialogTypes from "../../constants/DialogTypes";

const MainStoreActions = {
    setActions(actions){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SET_ACTIONS,
            actions
        });
    },

    setClassrooms(classrooms){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SET_CLASSROOMS,
            classrooms
        });
    },

    showClassroomsSelectDialog(){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SHOW_CLASSROOMS_SELECT_DIALOG
        });
    },

    hideClassroomsSelectDialog(){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.HIDE_CLASSROOMS_SELECT_DIALOG
        });
    },

    showConfirmDialog(message, onYes, onNo){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SHOW_CONFIRM_DIALOG,
            message,
            onYes,
            onNo
        });
    },

    hideConfirmDialog(){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.HIDE_CONFIRM_DIALOG
        });
    },

    updateVideoConstraints(videoConstraints){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.UPDATE_VIDEO_CONSTRAINTS,
            videoConstraints
        });
    },

    setLastRecognizedUser(user){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SET_LAST_RECOGNIZED_USER,
            user
        });
    },

    setWarn(warn){
        AppDispatcher.dispatch({
            type: MainStoreActionTypes.SET_WARN,
            warn
        })
    }

};

export default MainStoreActions;
