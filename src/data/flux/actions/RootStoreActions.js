import AppDispatcher from "../AppDispatcher";
import DialogTypes from "../../constants/DialogTypes";
import RootStoreActionTypes from "../actionTypes/RootStoreActionTypes";

const RootStoreActions = {

    setAuthenticatedUser(user){
        AppDispatcher.dispatch({
            type: RootStoreActionTypes.SET_AUTHENTICATED_USER,
            user
        })
    },

    showErrorDialog(message){
        AppDispatcher.dispatch({
            type: RootStoreActionTypes.SHOW_MODAL_DIALOG,
            dialogType: DialogTypes.ERROR,
            message
        })
    },

    showInfoDialog(message){
        AppDispatcher.dispatch({
            type: RootStoreActionTypes.SHOW_MODAL_DIALOG,
            dialogType: DialogTypes.INFO,
            message
        })
    },

    showConfirmDialog(message, onYes, onNo){
        AppDispatcher.dispatch({
            type: RootStoreActionTypes.SHOW_MODAL_DIALOG,
            dialogType: DialogTypes.CONFIRM,
            message,
            onYes,
            onNo
        })
    },

    hideModalDialog(){
        AppDispatcher.dispatch({
            type: RootStoreActionTypes.HIDE_MODAL_DIALOG
        })
    }
};

export default RootStoreActions;
