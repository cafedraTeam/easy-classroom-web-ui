import {Dispatcher} from 'flux';

//export default new Dispatcher();

class MyDisp extends Dispatcher {

    dispatchTockens = [];

    dispatch(payload) {
        console.log(payload);
        super.dispatch(payload);
    }

    register(callback) {
        const token = super.register(callback);
        this.addToken(token);
        return token;
    }

    addToken(dispatchToken){
        if(!this.dispatchTockens.includes(dispatchToken))
            this.dispatchTockens.push(dispatchToken);
    }

    removeToken(dispatchToken){
        const index = this.dispatchTockens.indexOf(dispatchToken);
        this.dispatchTockens.splice(index, 1);
    }

}

export default new MyDisp();