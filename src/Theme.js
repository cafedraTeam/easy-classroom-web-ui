const Theme = {
    palette: {
        primary: 'black',
        secondary: 'white',
    },
    spacing: {
        unit: 10
    }
};

export default Theme;