import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import SimpleSiteWrapper from "./SimpleSiteWrapper";
import SignIn from "./pages/signin/SignIn";
import Main from "./pages/main/Main";
import FrontendRoutes from "./data/constants/FrontendRoutes";
import ModalDialog from "./ModalDialog";
import RootStore from "./data/flux/stores/RootStore";
import StoreEvents from "./data/flux/StoreEvents";

export default class AppRoutes extends React.Component {

    
    state = RootStore.getStateJS();

    componentDidMount() {
        RootStore.on(StoreEvents.ROOT_STORE_UPDATED, this.updateState);
    }

    componentWillUnmount() {
        RootStore.removeListener(StoreEvents.ROOT_STORE_UPDATED, this.updateState);
    }

    updateState = () => {
        console.log(RootStore.getStateJS().showModalDialog);
        this.setState(RootStore.getStateJS());
    };

    render() {
        return (<BrowserRouter>
            {!this.state.authenticatedUser &&
            <SimpleSiteWrapper>
                <Switch>
                    <Route exact path={FrontendRoutes.SIGN_IN} component={() => <SignIn/>}/>
                    <Route exact path={FrontendRoutes.MAIN_PAGE} component={() => <Main/>}/>
                    <Route component={() => <h2>Page not found =(</h2>}/>
                </Switch>
            </SimpleSiteWrapper>
            }
            {this.state.authenticatedUser &&
            <h1>Authenticated by {JSON.stringify(this.state.authenticatedUser)}</h1>
            }
            <ModalDialog open={this.state.showModalDialog}
                         type={this.state.dialogType}
                         message={this.state.message}
                         onYes={this.state.onYes}
                         onNo={this.state.onNo}
            />
        </BrowserRouter>);
    }

}
