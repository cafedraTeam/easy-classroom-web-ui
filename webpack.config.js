const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path'),

    jsPath = './src',
    distPath = '../easyclassroombackend/main/static/',
    srcPath = path.join(__dirname, jsPath);

module.exports = {
    optimization: {
        minimize: false,
    },
    mode: "development",
    output: {
        path: path.resolve(__dirname, distPath),
        filename: `bundle.js`
    },
    entry: {
        test: [path.join(srcPath, '/index.js')]
    },
    watch: true,
    context: srcPath,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['babel-loader']
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)$/,
                loader: 'url-loader?limit=8192'
            }
        ]
    }
};