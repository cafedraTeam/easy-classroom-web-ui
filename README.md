#Easy classroom (web frontend)


*To launch instantly*:

* **copy this repo**

* **yarn dev** - with ```webpack-dev-server``` (chrome autoopen, hot reload)


*Open it from backend project*

* **clone this repo**

* **clone backend repo** ```ggit clone https://svyatoslav00012@bitbucket.org/cafedraTeam/easyclassroombackend.git```

* **yarn backdev** - will handle changes and rebuild bundle automatically

* **make symlink** - make main/static from backend link to ./dist from frontend 

    * Windows: ```mklink /D {backend_path}/main/ui/ {frontend_path}/dist/ ```
	
    * Mac OS/Linux: ```ln -s {frontend}/dist/ {backend_path}/main/ui```
	
    * Couldn't create link or it's broken? Try absolute path
	
    * Why? - In development we will simply inject files there, but as for now, it's the best and fastest solution.
	
    You change the code in react project - it immediately changes and backend now returns new page.
	
* **Done! You can work!** - just write somethink, you will see instant changes from backend


####Happy coding!!!